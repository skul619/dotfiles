#!/bin/bash

# Habilita e inicia o serviço de atualização de cache do DNF
sudo systemctl enable --now dnf-makecache.timer

# Adiciona as configurações no arquivo /etc/dnf/dnf.conf
sudo tee -a /etc/dnf/dnf.conf > /dev/null <<EOT
timeout=10
minrate=50000
fastestmirror=true
max_parallel_downloads=20
installonly_limit=10
deltarpm=true
EOT

# Atualiza o sistema
sudo dnf update -y

# Instalação do Vim
sudo dnf install vim -y

# Habilita o repositório RPM Fusion
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
sudo dnf install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y

# Adiciona o repositório do Visual Studio Code manualmente
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" | sudo tee /etc/yum.repos.d/vscode.repo > /dev/null

# Atualiza os repositórios
sudo dnf check-update

# Instalação de ferramentas de desenvolvimento
sudo dnf groupinstall "Development Tools" -y
sudo dnf install wget curl git -y

# Instalação do Visual Studio Code
sudo dnf install code -y

# Instalação do Gnome Tweaks e Gnome Extensions
sudo dnf install gnome-tweaks gnome-extensions-app -y

# Instalação de outras ferramentas
sudo dnf install cava mpv qbittorrent neofetch -y

# Instalação de aplicativos de comunicação
sudo dnf install telegram-desktop discord -y

# Download e instalação do Slack
wget -O slack.rpm https://downloads.slack-edge.com/linux_releases/slack-4.21.0-0.1.fc21.x86_64.rpm
sudo dnf install ./slack.rpm -y
rm slack.rpm

# Instalação do Virtualization
sudo dnf install @virtualization -y

# Inicia e habilita o serviço libvirtd
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

# Instalação do UFW (Uncomplicated Firewall)
sudo dnf install ufw -y

# Configuração das regras de firewall
sudo ufw limit 22/tcp
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw default deny incoming
sudo ufw default allow outgoing

# Habilita e inicia o UFW
sudo systemctl enable ufw
sudo ufw enable

# Instalação do Fail2ban
sudo dnf install fail2ban -y

# Habilita o serviço Fail2ban
sudo systemctl enable fail2ban

# Configuração do arquivo fail2ban.conf
sudo tee /etc/fail2ban/fail2ban.conf > /dev/null <<EOT
[DEFAULT]
ignoreip = 127.0.0.1/8 ::1
bantime = 3600
findtime = 600
maxretry = 5

[sshd]
enabled = true
EOT

# Instalação do Docker
sudo dnf install docker -y
sudo systemctl start docker
sudo systemctl enable docker

# Instalação do Python
sudo dnf install python3 -y

# Download e instalação das fontes FiraCode
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/FiraCode.zip
unzip FiraCode.zip -d FiraCode
sudo mv FiraCode/*.ttf /usr/share/fonts/
rm -rf FiraCode.zip FiraCode

# Geração da chave SSH
ssh-keygen -t rsa -b 4096 -C "skul619@gmail.com" -f ~/.ssh/id_rsa_skul619 -N ""

# Instalação do Zsh4Humans
if command -v curl >/dev/null 2>&1; then
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/romkatv/zsh4humans/v5/install)"
else
  sh -c "$(wget -O- https://raw.githubusercontent.com/romkatv/zsh4humans/v5/install)"
fi

# Configuração do atalho Super + Enter para abrir o Terminal GNOME
gsettings set org.gnome.settings-daemon.plugins.media-keys terminal "['<Super>Return']"

echo "Instalação concluída."

